PYTHON_VERSION ?= 3.10
NODE_VERSION ?= 16
DOCKER_IMAGE ?= okdocker/pynode-ng:$(PYTHON_VERSION)-$(NODE_VERSION).x

.PHONY: build push

push: build
	docker push $(DOCKER_IMAGE)

build:
	python dockerfile.py --python-version=$(PYTHON_VERSION) --node-version=$(NODE_VERSION) | docker build - -t $(DOCKER_IMAGE)

